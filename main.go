package main

import (
	"archive/zip"
	"bufio"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type Configuration struct {
	Version     int    `json:"version"`
	SourceUrl   string `json:"source_url"`
	InstallPath string `json:"install_path"`
	HttpTimeout int    `json:"http_timeout"`
}

type ElvUI struct {
	Id             int
	Name           string
	Author         string
	Url            string
	Version        string
	Changelog      string
	Ticket         string
	Git            string
	Patch          string
	Lastupdate     string
	Web_url        string
	Lastdownload   string
	Donate_url     string
	Small_desc     string
	Screenshot_url string
	Downloads      int
	Category       string
}

type VersionInfo struct {
	Major, Minor int
}

func (v VersionInfo) String() string {
	return strconv.Itoa(v.Major) + "." + strconv.Itoa(v.Minor)
}

func (version VersionInfo) Newer(other VersionInfo) bool {
	if version.Major > other.Major {
		return true
	} else if version.Major == other.Major {
		if version.Minor > other.Minor {
			return true
		}
	}
	return false

}

const programVersion = 3
const defaultSourceUrl = "https://www.tukui.org/api.php?ui=elvui"
const defaultHttpTimeout = 60

func getDefaultConfig() Configuration {
	return Configuration{Version: programVersion, SourceUrl: defaultSourceUrl, HttpTimeout: defaultHttpTimeout}
}

func getUserConfigDir() string {
	configPath, err := os.UserConfigDir()
	if err != nil {
		panic(err)
	}
	configPath = filepath.Join(configPath, "gecl")
	os.MkdirAll(configPath, 0750)

	return configPath
}

func getConfigFilePath() string {
	configPath := getUserConfigDir()
	configFile := filepath.Join(configPath, "config.json")

	return configFile
}

func getConfig() Configuration {
	// get user config dir and create (if not exists) the config dir for gecl
	configFile := getConfigFilePath()

	// if config file does not exist, create basic config and save it to disk. return created default config
	if _, err := os.Stat(configFile); errors.Is(err, os.ErrNotExist) {
		config := getDefaultConfig()
		persistConfig(configFile, config)
		return config
	}

	// if the file exists read it from disk, unmarshal and return it
	fileContent, err := os.ReadFile(configFile)
	if err != nil {
		// if we cannot read the config from disk, return a default config
		return getDefaultConfig()
	}
	configuration := Configuration{}
	// lookup if json decoder is better used here
	json.Unmarshal(fileContent, &configuration)
	// if the config from disk is not supported discard old and create a new
	if configuration.Version != programVersion {
		log.Printf("Unsupported config version. Required version %d, but found version %d", programVersion, configuration.Version)
		log.Println("Discarding old config and generating a new one.")
		config := getDefaultConfig()
		persistConfig(configFile, config)
		return config
	}
	return configuration
}

func persistConfig(configFile string, config Configuration) {
	fh, err := os.Create(configFile)
	if err != nil {
		panic(err)
	}
	defer fh.Close()
	encoder := json.NewEncoder(fh)
	encoder.Encode(&config)
	fmt.Println("Persisting config to disk")
}

func main() {
	fmt.Println("gecl --- go elvui client release", programVersion)
	config := getConfig()

	// load ElvUI info from api
	fmt.Println("Querying", config.SourceUrl)
	elvuiClient := http.Client{Timeout: time.Second * time.Duration(config.HttpTimeout)}
	res, err := elvuiClient.Get(config.SourceUrl)
	if err != nil {
		panic(err)
	}
	if res.Body != nil {
		defer res.Body.Close()
	}
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		panic(readErr)
	}
	elvui := ElvUI{}
	if err := json.Unmarshal(body, &elvui); err != nil {
		panic(err)
	}
	fmt.Printf("Found %s version %s, last update %s\n", elvui.Name, elvui.Version, elvui.Lastupdate)

	// get installed version
	if len(config.InstallPath) == 0 {
		fmt.Println("install path of ElvUI not set. Input install path:")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		tempPath := scanner.Text()
		tempPath = strings.Trim(tempPath, "\"")
		tempPath = strings.ReplaceAll(tempPath, "\\", "/")
		config.InstallPath = tempPath
		persistConfig(getConfigFilePath(), config)
	}
	elvuiTOC := filepath.Join(config.InstallPath, "ElvUI", "ElvUI_Mainline.toc")
	fmt.Println("Parsing installed ElvUI version from", elvuiTOC)

	installedVersion, _ := getInstalledVersion(elvuiTOC)
	var newInstall bool
	if installedVersion.Major == 0 && installedVersion.Minor == 0 {
		fmt.Println("No installed version found")
		newInstall = true
	} else {
		fmt.Println("Found installed version", installedVersion.String())
		newInstall = false
	}
	newVersion := convertToVersionInfo(elvui.Version)

	if newVersion.Newer(installedVersion) {
		fmt.Printf("Update available: %s -> %s\n", installedVersion.String(), newVersion.String())
		doUpdate := askForConfirmation("Do you want to install?")
		if !doUpdate {
			fmt.Println("Update canceled by user")
			os.Exit(0)
		}
		// download zip file to user temp dir
		fmt.Println("Downloading update ...")
		tempFile, err := os.CreateTemp("", "gecl-temp.*.zip")
		if err != nil {
			panic(err)
		}
		defer cleanup(tempFile)
		resp, err := elvuiClient.Get(elvui.Url)
		if err != nil {
			panic(err)
		}
		defer resp.Body.Close()
		size, err := io.Copy(tempFile, resp.Body)
		if err != nil {
			panic(err)
		}
		fmt.Printf("Downloaded file %s with size %d kb\n", tempFile.Name(), size/1024)
		if !newInstall {
			backupCurrentInstallation(config)
		}

		// unpack new version and copy into target

		unzip(tempFile.Name(), config.InstallPath, tempFile.Name())
	} else {
		fmt.Println("Newest version already installed. Doing nothing")
	}

}

func backupCurrentInstallation(config Configuration) {
	// backup old installed version
	// create backup zip file in addon folder
	backupZipName := filepath.Join(getUserConfigDir(), "elvui-backup-"+time.Now().Format("20060102150405")+".zip")
	backupZipFile, err := os.Create(backupZipName)
	if err != nil {
		panic(err)
	}
	defer backupZipFile.Close()
	fmt.Printf("Creating backup to %s\n", backupZipName)
	zipWriter := zip.NewWriter(backupZipFile)
	defer zipWriter.Close()
	walker := func(path string, info os.FileInfo, err error) error {
		//fmt.Printf("Crawling: %#v\n", path)
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		file, err := os.Open(path)
		if err != nil {
			return err
		}
		defer file.Close()

		var relativePath string
		if filepath.IsAbs(path) {
			relativePath, err = filepath.Rel(config.InstallPath, path)
			if err != nil {
				return err
			}
		} else {
			relativePath = path
		}
		f, err := zipWriter.Create(relativePath)
		if err != nil {
			return err
		}

		_, err = io.Copy(f, file)
		if err != nil {
			return err
		}

		return nil
	}

	elvuiFolders := []string{filepath.Join(config.InstallPath, "ElvUI"), filepath.Join(config.InstallPath, "ElvUI_Options"), filepath.Join(config.InstallPath, "ElvUI_Libraries")}
	for _, item := range elvuiFolders {
		walkError := filepath.Walk(item, walker)
		if walkError != nil {
			panic(walkError)
		}
		// delete addon folders
		fmt.Printf("Deleting folder %s\n", item)
		os.RemoveAll(item)
	}
}

// shamelessly borrowed from https://gosamples.dev/unzip-file/
func unzip(zipFile string, destination string, tempFileName string) error {
	fmt.Printf("Unzipping %s into %s\n", tempFileName, destination)
	// 1. Open the zip file
	reader, err := zip.OpenReader(zipFile)
	if err != nil {
		return err
	}
	defer reader.Close()

	// 2. Get the absolute destination path
	destination, err = filepath.Abs(destination)
	if err != nil {
		return err
	}

	// 3. Iterate over zip files inside the archive and unzip each of them
	for _, f := range reader.File {
		err := unzipFile(f, destination)
		if err != nil {
			return err
		}
	}

	return nil
}

func unzipFile(f *zip.File, destination string) error {
	// 4. Check if file paths are not vulnerable to Zip Slip
	filePath := filepath.Join(destination, f.Name)
	if !strings.HasPrefix(filePath, filepath.Clean(destination)+string(os.PathSeparator)) {
		return fmt.Errorf("invalid file path: %s", filePath)
	}

	// 5. Create directory tree
	if f.FileInfo().IsDir() {
		if err := os.MkdirAll(filePath, os.ModePerm); err != nil {
			return err
		}
		return nil
	}

	if err := os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
		return err
	}

	// 6. Create a destination file for unzipped content
	destinationFile, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
	if err != nil {
		return err
	}
	defer destinationFile.Close()

	// 7. Unzip the content of a file and copy it to the destination file
	zippedFile, err := f.Open()
	if err != nil {
		return err
	}
	defer zippedFile.Close()

	if _, err := io.Copy(destinationFile, zippedFile); err != nil {
		return err
	}
	return nil
}

// TODO: refactor to directly use the file contents instead of file name
func getInstalledVersion(elvuiTOC string) (version VersionInfo, err error) {
	toc, err := os.ReadFile(elvuiTOC)
	if err != nil {
		return VersionInfo{0, 0}, err
	}
	fileData := string(toc)
	temp := strings.Split(fileData, "\n")

	var versionLine string
	for _, item := range temp {
		if strings.Contains(item, "Version:") {
			versionLine = item
		}
	}

	version = getVersion(versionLine)
	err = nil
	return
}

func cleanup(tempFile *os.File) {
	tempFile.Close()
	fmt.Println("Deleting temp file", tempFile.Name())
	os.Remove(tempFile.Name())
}

func getVersion(versionString string) VersionInfo {
	// versionLine has this format: ## Version: 12.80
	// TODO sanitize array usage
	splittedLine := strings.Split(versionString, " ")
	version := VersionInfo{}
	version.Major, _ = strconv.Atoi(strings.Split(splittedLine[2], ".")[0])
	version.Minor, _ = strconv.Atoi(strings.Split(splittedLine[2], ".")[1])

	return version
}

func convertToVersionInfo(versionString string) VersionInfo {
	version := VersionInfo{}
	version.Major, _ = strconv.Atoi(strings.Split(versionString, ".")[0])
	version.Minor, _ = strconv.Atoi(strings.Split(versionString, ".")[1])

	return version
}

func askForConfirmation(s string) bool {
	reader := bufio.NewReader(os.Stdin)

	for i := 0; i < 3; i++ {
		fmt.Printf("%s [Y/n]: ", s)

		response, err := reader.ReadString('\n')
		if err != nil {
			panic(err)
		}

		response = strings.ToLower(strings.TrimSpace(response))

		if response == "y" || response == "yes" || response == "" {
			return true
		} else if response == "n" || response == "no" {
			return false
		}
	}
	return false
}
