# GECL --- Go ElvUI client

This is a simple update client for the popular World of Warcraft Interface replacement ElvUI (https://www.tukui.org/download.php?ui=elvui) written in Go and compatible with Window and Linux (possibly Mac too, but because I lack a Mac I cannot test this).

GECL is distributed under the MIT license. See [LICENSE](LICENSE.md) for additional information.

GECL stores its config in the system config dir and uses the user temp directory for downloading  temporary files.   
On Windows these are:
- c:\users\<USERNAME>\AppData\Roaming\gecl\config.json
- c:\users\<USERNAME>\AppData\Local\Temp

GECL creates a backup of the existing ElvUI installation into the config directory before extracting the new version. Therefore if something breaks you can manually revert to the old version. After GECL is finished with updating it cleans up after itself and deletes unused tempory files. GECL doesn't touch your ElvUI config, this is stored in a seperate directory within WoW and thus ignored by GECL.